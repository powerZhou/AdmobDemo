cc.Class({
    extends: cc.Component,

    properties: {
        label: {
            default: null,
            type: cc.Label
        },
        // defaults, set visually when attaching this script to the Canvas
        text: 'Hello, World!'
    },

    // use this for initialization
    onLoad: function () {
        this.label.string = this.text;
    },

    // called every frame
    update: function (dt) {

    },

    onBannerClick(){
        if(cc.AdmobAds.Banner){
            let adUnid = "ca-app-pub-3940256099942544/2934735716";
            if(cc.sys.os == cc.sys.OS_ANDROID){
                adUnid = "ca-app-pub-3940256099942544/6300978111";
            }
           let banner = new cc.AdmobAds.Banner(adUnid);
           banner.loadAd(); 
        }
    },
    
    onIntertitialClick(){
        if(cc.AdmobAds.Interstitial){
            let adUnid = "ca-app-pub-3940256099942544/5135589807";
            if(cc.sys.os == cc.sys.OS_ANDROID){
                adUnid = "ca-app-pub-3940256099942544/1033173712";
            }
            this.interstitial = new cc.AdmobAds.Interstitial(adUnid);
            this.interstitial.loadAd();
        }
    },

    onRewardedClick(){
        if(cc.AdmobAds.RewardedVideo){
            let adUnid = "ca-app-pub-3940256099942544/1712485313";
            if(cc.sys.os == cc.sys.OS_ANDROID){
                adUnid = "ca-app-pub-3940256099942544/5224354917";
            }
            this.rewarded = new cc.AdmobAds.RewardedVideo(adUnid);
            this.rewarded.loadAd();
        }
    },

    onShowClick(){
        if(this.interstitial){
            this.interstitial.show();
            this.interstitial = null;
        }

        if(this.rewarded){
            this.rewarded.show();
            this.rewarded = null;
        }
    },

    onShowTestSuite(){
        cc.AdmobAds.showMediationTestSuite();
    }
});
