//
//  AdmobHelper.m
//  admob-mobile
//
//  Created by shawn on 26/04/2019.
//

#import "AdmobHelper.h"
#import <Foundation/Foundation.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <GoogleMobileAdsMediationTestSuite/GoogleMobileAdsMediationTestSuite.h>
#include "cocos/scripting/js-bindings/jswrapper/SeApi.h"

typedef enum ADS_TYPE {
    BANNER = 1,
    REWARDEDVIDEO = 2,
    INTERSTITIAL = 3,
}ADS_TYPE;

typedef enum BANNER_POSITION {
    ALIGN_PARENT_TOP =  1,
    ALIGN_PARENT_BOTTOM = 2,
}BANNER_POSITION;

typedef enum BANNER_SIZE {
    AD_INTERSTITIAL = 1,
    BANNER_FULL = 2,
    BANNER_LARGE = 3,
    BANNER_SMART_PORTRAIT = 4,
    BANNER_SMART_LANDSCAPE = 5,
}BANNER_SIZE;

@interface AdmobHelper ()<GADBannerViewDelegate,GADInterstitialDelegate,GADRewardBasedVideoAdDelegate>

@property (nonatomic, strong) GADInterstitial *interstitialAd;
@property (nonatomic,strong)GADBannerView *adView;
@property (nonatomic,strong)UIWindow *window;
@property (nonatomic,strong)NSString *mRewardedPlacementId;
@end
@implementation AdmobHelper
#pragma mark - AdmobHelper Method
-(AdmobHelper *)init{
    self = [super init];
    self.adList = [[NSMutableDictionary alloc] init];
    self.window = [[UIApplication sharedApplication] windows][0];
    self.mRewardedPlacementId = @"";
    [[GADMobileAds sharedInstance] startWithCompletionHandler:nil];
    return self;
}

-(id)getAd:(NSString *)placementId{
    return [self.adList objectForKey:placementId];;
}

-(id)removeAd:(NSString *)placementId{
    id ad = [self.adList objectForKey:placementId];
    if(ad != NULL){
        [self.adList removeObjectForKey:placementId];
    }
    return ad;
}

-(void) show:(NSString *)placementId{
    id ad =[self getAd:placementId];
    if(ad != NULL){
        if([ad isKindOfClass:[GADInterstitial class]]){
            GADInterstitial *interAd = (GADInterstitial *)ad;
            if(interAd.isReady){
                [interAd presentFromRootViewController:self.window.rootViewController];
            }else{
                NSLog(@"interstitial is ready false");
            }
        }else if([ad isKindOfClass:[GADRewardBasedVideoAd class]]){
            if([[GADRewardBasedVideoAd sharedInstance] isReady]){
                [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:self.window.rootViewController];
            }else{
                NSLog(@"GADRewardBasedVideoAd is ready false");
            }
        }
    }
}

-(void)showTestSuite{
    NSString *file = [[NSBundle mainBundle]pathForResource:@"Info" ofType:@"plist"];
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithContentsOfFile:file];
    NSString *appId =[dict objectForKey:@"GADApplicationIdentifier"];
    [GoogleMobileAdsMediationTestSuite presentWithAppID:appId onViewController:self.window.rootViewController delegate:nil];
}

-(void) load:(NSString *)placementId{
    id ad =[self getAd:placementId];
    if(ad != NULL){
        if([ad isKindOfClass:[GADInterstitial class]]){
            GADInterstitial *interAd = (GADInterstitial *)ad;
            GADRequest *request = [GADRequest request];
            [interAd loadRequest:request];
        }else if([ad isKindOfClass:[GADRewardBasedVideoAd class]]){
           
        }else if([ad isKindOfClass:[GADBannerView class]]){
            GADBannerView *banner = (GADBannerView *)ad;
            [banner loadRequest:[GADRequest request]];
        }
    }
}

-(bool)_isExist:(NSString *)placementId{
    return [self.adList objectForKey:placementId] != NULL;
}

-(void)destroy:(NSString *)placementId{
    id ad = [self getAd:placementId];
    if(ad != NULL && [ad isKindOfClass:[GADBannerView class]]){
        GADBannerView *banner = (GADBannerView *)ad;
        [banner removeFromSuperview];
    }
}

-(void)addRewardAd:(NSString *)placementId{
    GADRequest *request = [GADRequest request];
    request.testDevices = @[ kGADSimulatorID ];
    self.mRewardedPlacementId = placementId;
    [[GADRewardBasedVideoAd sharedInstance] loadRequest:request withAdUnitID:placementId];
    [GADRewardBasedVideoAd sharedInstance].delegate = self;
    [self.adList setValue:[GADRewardBasedVideoAd sharedInstance] forKey:placementId];
    [self handlerMessageToGame:@"onCreated" withAdsId:placementId];
}

-(void)addInterstitalAd:(NSString *)placementId{
    GADInterstitial *view = [[GADInterstitial alloc] initWithAdUnitID:placementId];
    view.delegate = self;
    [self.adList setValue:view forKey:placementId];
    [self handlerMessageToGame:@"onCreated" withAdsId:placementId];
}

-(void)addBanner:(NSString *)placementId withSize:(NSString *)size andPosition:(NSString *)position{
    int intSize = [size intValue];
    GADAdSize adSize = kGADAdSizeBanner;
    switch (intSize) {
        case AD_INTERSTITIAL:
            adSize = kGADAdSizeFluid;
            break;
        case BANNER_FULL:
            adSize = kGADAdSizeFullBanner;
            break;
        case BANNER_LARGE:
            adSize = kGADAdSizeLargeBanner;
        case BANNER_SMART_PORTRAIT:
            adSize = kGADAdSizeSmartBannerPortrait;
        case BANNER_SMART_LANDSCAPE:
            adSize = kGADAdSizeSmartBannerLandscape;
            break;
    }
    
    GADBannerView *ad = [[GADBannerView alloc] initWithAdSize:adSize];
    ad.adUnitID = placementId;
    ad.delegate = self;
    ad.rootViewController = self.window.rootViewController;
    ad.translatesAutoresizingMaskIntoConstraints = false;
    [self.adList setValue:ad forKey:placementId];
    [self.window.rootViewController.view addSubview:ad];
    
    //安全域设置
    if(@available(ios 11.0,*)){
        [self positionBannerViewFullWidthAtBottomOfSafeArea:ad];
    }else{
        [self positionBannerViewFullVidthAtBottomOfView:ad];
    }

    [self handlerMessageToGame:@"onCreated" withAdsId:placementId];
}

-(void)positionBannerViewFullWidthAtBottomOfSafeArea:(UIView *_Nonnull)bannerView NS_AVAILABLE_IOS(11.0){
    UILayoutGuide *guide = self.window.rootViewController.view.safeAreaLayoutGuide;
    [NSLayoutConstraint activateConstraints:@[
                                              [guide.leftAnchor constraintEqualToAnchor:bannerView.leftAnchor],
                                              [guide.rightAnchor constraintEqualToAnchor:bannerView.rightAnchor],
                                              [guide.bottomAnchor constraintEqualToAnchor:bannerView.bottomAnchor]
                                              ]];
}

-(void)positionBannerViewFullVidthAtBottomOfView:(UIView *_Nonnull)bannerView{
    [self.window.rootViewController.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.window.rootViewController.view attribute:NSLayoutAttributeLeading multiplier:1 constant:0]];
    [self.window.rootViewController.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.window.rootViewController.view attribute:NSLayoutAttributeTrailing multiplier:1 constant:0]];
    [self.window.rootViewController.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.window.rootViewController.view attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
}

-(void)handlerMessageToGame :(NSString *)eventName withAdsId:(NSString *)placementId{
    NSString *execStr = [NSString stringWithFormat:@"cc.AdmobAds._eventReceiver('%@','%@')",eventName,placementId];
    se::ScriptEngine::getInstance()->evalString([execStr UTF8String]);
}

-(void)handlerMessageToGame :(NSString *)eventName withAdsId:(NSString *)placementId andErrorCode:(NSString *)code{
    NSString *execStr = [NSString stringWithFormat:@"cc.AdmobAds._eventReceiver('%@','%@','%@')",eventName,placementId,code];
    se::ScriptEngine::getInstance()->evalString([execStr UTF8String]);
}

#pragma mark - AdmobHelper static Methods
+ (AdmobHelper *)getInstance
{
    static AdmobHelper *adInstance;
    
    @synchronized(self)
    {
        if (!adInstance){
            adInstance = [[AdmobHelper alloc] init];
        }
        return adInstance;
    }
}

+(void)showMediationTestSuite{
    [[AdmobHelper getInstance] showTestSuite];
}

+(void)loadAd:(NSString *)placementId{
    [[AdmobHelper getInstance] load:placementId];
}
+(void)showAd:(NSString *)placementId{
    [[AdmobHelper getInstance] show:placementId];
}
+(void)destroyAd:(NSString *)placementId{
    [[AdmobHelper getInstance] destroy:placementId];
}

+(void)createAd:(NSString *)placementId withSize:(NSString *)size andPosition:(NSString *)position{
    [[AdmobHelper getInstance] addBanner:placementId withSize:size andPosition:position];
}

+(void)createAd:(NSString *)type withId:(NSString *)placementId{
    if([type intValue] == INTERSTITIAL){
        [[AdmobHelper getInstance] addInterstitalAd:placementId];
    }else if([type intValue] == REWARDEDVIDEO){
        [[AdmobHelper getInstance] addRewardAd:placementId];
    }
}


#pragma mark - GADInterstitialDelegate implementation
-(void)interstitialDidReceiveAd:(GADInterstitial *)ad{
    NSLog(@"interstitial ad was loaded. Can present now.");
    [self handlerMessageToGame:@"onAdLoaded" withAdsId:ad.adUnitID];
}

-(void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error{
   NSLog(@"interstitial ad failed to load with error: %@", error.description);
     NSString *errorCode = [NSString stringWithFormat:@"%ld",(long)error.code];
    [self handlerMessageToGame:@"onError" withAdsId:ad.adUnitID andErrorCode:errorCode];
}

-(void)interstitialWillDismissScreen:(GADInterstitial *)ad{
    NSLog(@"interstitialWillDismissScreen");
}

-(void)interstitialWillPresentScreen:(GADInterstitial *)ad{
    NSLog(@"interstitialWillPresentScreen");
}

-(void)interstitialWillLeaveApplication:(GADInterstitial *)ad{
    NSLog(@"interstitialWillLeaveApplication");
    [self handlerMessageToGame:@"onAdLeave" withAdsId:ad.adUnitID];
}

#pragma mark - FBRewardedVideoAdDelegate implementation

-(void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd{
    NSLog(@"reward base video was Receive.");
    [self handlerMessageToGame:@"onAdLoaded" withAdsId:self.mRewardedPlacementId];
}

-(void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd{
     NSLog(@"rewardBasedVideoAdDidOpen");
}

-(void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd{
    NSLog(@"rewardBasedVideoAdDidClose");
}

-(void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd{
      NSLog(@"rewardBasedVideoAdWillLeaveApplication");
}

-(void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd didRewardUserWithReward:(GADAdReward *)reward{
    NSLog(@"rewardBasedVideoAd didRewardUserWithReward");
    [self handlerMessageToGame:@"onReward" withAdsId:self.mRewardedPlacementId];
}

-(void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd didFailToLoadWithError:(NSError *)error{
    NSLog(@"rewardBasedVideoAd failed to load with error: %@", [error localizedDescription]);
    NSString *errorCode = [NSString stringWithFormat:@"%ld",(long)error.code];
    [self handlerMessageToGame:@"onError" withAdsId:self.mRewardedPlacementId andErrorCode:errorCode];
}

#pragma mark - AdmobBannerViewDelegate implementation

// Implement this function if you want to change the viewController after the FBAdView
// is created. The viewController will be used to present the modal view (such as the
// in-app browser that can appear when an ad is clicked).
//- (UIViewController *)viewControllerForPresentingModalView
//{
//    return self;
//}

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView
{
    NSLog(@"Ad was Receive.");
    [self handlerMessageToGame:@"onAdLoaded" withAdsId:bannerView.adUnitID];
}

-(void)adViewWillPresentScreen:(GADBannerView *)bannerView{
    NSLog(@"adViewWillPresentScreen");
    [self handlerMessageToGame:@"onAdWillPresentScreen" withAdsId:bannerView.adUnitID];
}

- (void)adViewWillDismissScreen:(GADBannerView *)bannerView
{
    NSLog(@"adViewWillDismissScreen");
    [self handlerMessageToGame:@"onAdWillDismissScreen" withAdsId:bannerView.adUnitID];
}

-(void)adViewDidDismissScreen:(GADBannerView *)bannerView{
    NSLog(@"adViewDidDismissScreen");
    [self handlerMessageToGame:@"onAdDidDismissScreen" withAdsId:bannerView.adUnitID];
}

-(void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(nonnull GADRequestError *)error
{
    NSLog(@"Ad failed to load with error: %@", [error localizedDescription]);
    NSString *errorCode = [NSString stringWithFormat:@"%ld",(long)error.code];
    [self handlerMessageToGame:@"onError" withAdsId:adView.adUnitID andErrorCode:errorCode];
}

-(void)adViewWillLeaveApplication:(GADBannerView *)bannerView{
    NSLog(@"adViewWillLeaveApplication");
    [self handlerMessageToGame:@"onAdWillLeaveApplication" withAdsId:bannerView.adUnitID];
}


@end
